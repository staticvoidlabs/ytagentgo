#!/bin/bash

echo "yda.service: ## Starting ##" | systemd-cat -p info

cd /home/alex/bin/yda
./ytagentgo

echo "yda.service: Shutting down." | systemd-cat -p info
