package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/staticvoidlabs/ytagentgo/managers"
	"gitlab.com/staticvoidlabs/ytagentgo/models"
)

//var mShouldExit = false
var mCurrentConfig models.Config

func main() {

	// Init config.
	mCurrentConfig = managers.GetCurrentConfig()

	// Check if running in debug mode.
	if len(os.Args) > 1 && os.Args[1] == "debug" {
		fmt.Println("Running in debug mode!")
		managers.SessionType = "debug"
	}

	managers.WriteLogMessage("Starting Youtube Download Agent (Version: "+mCurrentConfig.Version+")...", true)
	managers.WriteLogMessage("Found "+strconv.Itoa(len(managers.GetChannelList()))+" channels in config.json file.", false)

	// Initailize repositories.
	// managers.InitRepositories()

	// Initailize GorillaMUX service and start listening.
	managers.InitAPIService()

}
