package managers

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/acme/autocert"
)

var mLastVideoParameter string = ""

// InitAPIService starts the API service.
func InitAPIService() {

	WriteLogMessage("Starting Web API services...", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", generateHTMLOutput)
	router.HandleFunc("/login", login)
	router.HandleFunc("/login/{key}", login)
	router.HandleFunc("/logout", logout)
	router.HandleFunc("/yda", generateHTMLOutput)
	router.HandleFunc("/yda/videos", generateHTMLOutput)
	router.HandleFunc("/yda/queue", showCurrentQueue)
	router.HandleFunc("/yda/queue/log", showCurrentQueue)
	router.HandleFunc("/yda/queue/add/{id}", addQueueItem)
	router.HandleFunc("/yda/queue/cancel", cancelDownloads)
	router.HandleFunc("/yda/log", getStateInfoService)
	router.HandleFunc("/yda/ctrl/restart", restartSubsystem)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	// Start rest service.

	var err error = nil

	tmpConfiguredApiPort := ":" + strconv.Itoa(mCurrentConfig.ApiPort)

	if SessionType == "debug" || !mCurrentConfig.UseSSL {

		err = http.ListenAndServe(tmpConfiguredApiPort, router)

	} else {

		// *** HTTPS with Let's Encrypt cert. ***

		// Start HTTP-Server for Let's Encrypt challanges.
		tmpCertManager := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			Cache:      autocert.DirCache(mCurrentConfig.CertsDir),
			HostPolicy: autocert.HostWhitelist("fatseven.com", "www.fatseven.com"),
		}

		go http.ListenAndServe(":8181", tmpCertManager.HTTPHandler(nil))

		// Start HTTPS-Server.
		tmpServer := &http.Server{
			Addr:    tmpConfiguredApiPort,
			Handler: router,
			TLSConfig: &tls.Config{
				GetCertificate: tmpCertManager.GetCertificate,
			},
		}

		err = tmpServer.ListenAndServeTLS("", "")

		// *** HTTPS with self signed cert. ***
		//err = http.ListenAndServeTLS(tmpConfiguredApiPort, mCurrentConfig.CacheDir+"/server.crt", mCurrentConfig.CacheDir+"/server.key", router)

	}

	if err != nil {
		fmt.Println("Error starting Web API services: ")
		fmt.Println(err)
	}

}

// Endpoint implementation.
func login(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:login", true)

	// Check if key parameter exists.
	params := mux.Vars(r)
	tmpKey := params["key"]

	if tmpKey != "" {

		authenticateUser(w, r, tmpKey)

		// We will redirect in JS because this here doesn't work. Maybe because we already wrote someting to 'w' in authenticateUser().
		//http.Redirect(w, r, "/yda/log", http.StatusSeeOther)

		return

	} else {

		// Generate the HTML content.
		tmpTemplate, err := template.ParseFiles("www/login.html")

		if err != nil {
			json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:error: " + err.Error())
		}

		tmpTemplate.Execute(w, mVideoRepository)

	}
}

func logout(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:logout", true)

	resetSession(w, r)

	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	tmpLogMessages := GetCurrentLog()

	for _, message := range tmpLogMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func showCurrentQueue(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:showCurrentQueue", true)

	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	json.NewEncoder(w).Encode("VoidServices_response:show_current_queue:success.")
}

func restartSubsystem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartSubsystem", true)

	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	json.NewEncoder(w).Encode("VoidServices_response:restart_subsystem:success.")
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateHTMLOutput", true)

	// Check if session is authentcated.
	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	// Build view model object.
	mViewModelMainSite.Videos = mVideoRepository.Videos
	mViewModelMainSite.RefreshedAt = mVideoRepository.RefreshedAt
	mViewModelMainSite.Jobs = mDownloadQueue

	// Populate repo with videos if no downlaod is running.
	if mRunningJobs == 0 {
		mViewModelMainSite.IsJobRunning = false
		InitRepositories()
	} else {
		mViewModelMainSite.IsJobRunning = true
	}

	// Now generate the HTML content.
	tmpTemplate, err := template.ParseFiles("www/index.html")

	if err != nil {
		json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:error: " + err.Error())
	}

	tmpTemplate.Execute(w, mViewModelMainSite)
}

func addQueueItem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:addQueueItem", true)

	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	// Get videoID parameter.
	params := mux.Vars(r)
	videoID := params["id"]

	processDownloadRequest(videoID)

	json.NewEncoder(w).Encode("VoidServices_response:add_queue_item:success.")

}

func cancelDownloads(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:cancelDownloads", true)

	tmpIsAuthenticated := checkAuthState(w, r)
	if !tmpIsAuthenticated {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	json.NewEncoder(w).Encode("VoidServices_response:cancel_downloads:success.")

}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:testing", true)

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}

func processDownloadRequest(videoID string) {

	if mLastVideoParameter != videoID {

		mLastVideoParameter = videoID

		// This source check is not needed because we already have the videoID.
		/*
			// Get first 23 chars of current clipboard content.
			tmpRunes := []rune(videoID)
			tmpSubstringStart := string(tmpRunes[0:23])

			if tmpSubstringStart != "https://www.youtube.com" {
				//mLastAction = "skipping: " + tmpClipboardContent
				return
			}
		*/

		go BuildDownloadJob(videoID, mCurrentConfig)
	}
}
