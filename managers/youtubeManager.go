package managers

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	duration "github.com/channelmeter/iso8601duration"
	"gitlab.com/staticvoidlabs/ytagentgo/models"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

var mError error

const cYOUTUBE_VIDEO_BASE_URL = "https://www.youtube.com/watch?v="

func GetVideoListByChannelID(channelID string) ([]models.YtVideo, error) {

	if mCurrentConfig.DebugLevel == 1 {

		var tmpVideoSlice []models.YtVideo

		var tmpVideoToAdd models.YtVideo

		tmpVideoToAdd.ChannelTitle = "Debug ChannelTitle"
		tmpVideoToAdd.Title = "Debug Title"
		tmpVideoToAdd.VideoID = "ZAiq5fJtFog"
		tmpVideoToAdd.OriginalTitle = "Debug OriginalTitle"
		tmpVideoToAdd.PublishedAt = time.Now().String()
		tmpVideoToAdd.Duration = "0" //getVideoDurationByVideoID(e.Id.VideoId)
		tmpVideoToAdd.URL = cYOUTUBE_VIDEO_BASE_URL + "ZAiq5fJtFog"
		tmpVideoToAdd.FileName = "xxx.mp4"
		tmpVideoToAdd.ThumbURL = "Debug ThumbURL"
		tmpVideoToAdd.State = 0

		tmpVideoSlice = append(tmpVideoSlice, tmpVideoToAdd)

		return tmpVideoSlice, nil
	}

	ctx := context.Background()
	tmpYoutubeAPIKey := getYoutubeAPIKey()
	tmpYoutubeService, err := youtube.NewService(ctx, option.WithAPIKey(tmpYoutubeAPIKey))

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)

		return nil, errors.New("Error while initiating Youtibe API service.")
	}

	mContentArray := []string{"snippet"}

	tmpSearchVideoRequest := tmpYoutubeService.Search.List(mContentArray)
	tmpSearchVideoRequest.Order("date")
	tmpSearchVideoRequest.MaxResults(int64(mCurrentConfig.MaxVideosPerChannel))
	tmpSearchVideoRequest.ChannelId(channelID)
	tmpSearchVideoRequest.PublishedAfter(getFilterDate()) //  ("2022-01-22T09:00:00Z")

	tmpSearchVideoResponse, err := tmpSearchVideoRequest.Do()

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)

		return nil, errors.New("Error requesting video list via Youtibe API service.")
	}

	var tmpVideoSlice []models.YtVideo

	if tmpSearchVideoResponse.Items != nil && len(tmpSearchVideoResponse.Items) > 0 {

		for i, e := range tmpSearchVideoResponse.Items {

			if tmpSearchVideoResponse.Items[i] != nil {

				var tmpVideoToAdd models.YtVideo

				tmpVideoToAdd.ChannelTitle = e.Snippet.ChannelTitle
				tmpVideoToAdd.Title = e.Snippet.Title
				tmpVideoToAdd.VideoID = e.Id.VideoId
				tmpVideoToAdd.OriginalTitle = e.Snippet.Title
				tmpVideoToAdd.PublishedAt = getDatePublishedAtAsString(e.Snippet.PublishedAt)
				tmpVideoToAdd.Duration = getVideoDurationByVideoID(e.Id.VideoId)
				tmpVideoToAdd.URL = cYOUTUBE_VIDEO_BASE_URL + e.Id.VideoId
				tmpVideoToAdd.FileName = e.Snippet.Title
				tmpVideoToAdd.ThumbURL = e.Snippet.Thumbnails.Default.Url
				tmpVideoToAdd.State = 0

				tmpVideoSlice = append(tmpVideoSlice, tmpVideoToAdd)

				if mCurrentConfig.DebugLevel == 1 {

					WriteLogMessage("Found item > ChannelID: "+channelID+"   Channel: "+tmpVideoToAdd.ChannelTitle+"     Title: "+tmpVideoToAdd.Title, false)

				}
			}
		}
	}

	return tmpVideoSlice, nil
}

func getDatePublishedAtAsString(publishedAtOrig string) string {

	// YouTV:   2021-09-24T10:30:00.000+02:00
	// Youtube: 2022-01-15T11:00:09Z

	tmpDateString := ""

	tmpStringDate := publishedAtOrig[0:10]
	tmpStringTime := publishedAtOrig[11:19]

	tmpStringDate = strings.Replace(tmpStringDate, "-", "/", -1)

	tmpDateString = tmpStringDate + " " + tmpStringTime

	return tmpDateString
}

func getFilterDate() string {

	var tmpFilterDateTimeString = "2022-01-15T11:00:09Z"

	tmpDays := mCurrentConfig.ShowVideosOfLastDays * -1

	now := time.Now()
	tmpFilterDateTimeString = now.AddDate(0, 0, tmpDays).String()

	// we got: 2022-01-21 08:54:50.1226091 +0100 CET
	// and we need: 2022-01-15T11:00:09Z

	tmpStringDate := tmpFilterDateTimeString[0:10]
	tmpStringTime := tmpFilterDateTimeString[11:19]

	tmpFilterDateTimeString = tmpStringDate + "T" + tmpStringTime + "Z"

	return tmpFilterDateTimeString
}

func getVideoDurationByVideoID(videoID string) string {

	var tmpDuration string

	tmpVideo := GetVideosByID(videoID)

	tmpDuration = getDurationByIsoString(tmpVideo.Duration)

	return tmpDuration
}

// GetVideosByID returns a list of videos for a given ID.
func GetVideosByID(videoID string) models.YtVideo {

	//videoID = "1w2xlTMGVFU"

	var tmpReturnVideo models.YtVideo

	ctx := context.Background()
	tmpYoutubeAPIKey := getYoutubeAPIKey()
	tmpYoutubeService, err := youtube.NewService(ctx, option.WithAPIKey(tmpYoutubeAPIKey))

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)

		return tmpReturnVideo
	}

	mContentArray := []string{"snippet,contentDetails"}

	tmpSearchVideoRequest := tmpYoutubeService.Videos.List(mContentArray)
	tmpSearchVideoRequest.Id(videoID)

	tmpSearchVideoResponse, err := tmpSearchVideoRequest.Do()

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)

		return tmpReturnVideo
	}

	if tmpSearchVideoResponse.Items != nil && len(tmpSearchVideoResponse.Items) > 0 {

		tmpReturnVideo.ChannelTitle = tmpSearchVideoResponse.Items[0].Snippet.ChannelTitle
		tmpReturnVideo.Title = tmpSearchVideoResponse.Items[0].Snippet.Title
		tmpReturnVideo.OriginalTitle = tmpSearchVideoResponse.Items[0].Snippet.Title
		tmpReturnVideo.PublishedAt = tmpSearchVideoResponse.Items[0].Snippet.PublishedAt
		tmpReturnVideo.Duration = tmpSearchVideoResponse.Items[0].ContentDetails.Duration
		tmpReturnVideo.ThumbURL = tmpSearchVideoResponse.Items[0].Snippet.Thumbnails.Default.Url
		//tmpReturnVideo.ThumbFilepath = generateThumbPNG(tmpReturnVideo.ThumbURL, videoID)

		if err != nil {
			//panic(err)

			WriteLogMessage("Error: "+err.Error(), true)
		}

	}

	return tmpReturnVideo
}

// GetChannelsByID returns a list of channels for a given ID.
func _GetChannelsByID(channelID string, ytAPIKey string) {

	channelID = "UCBzai1GXVKDdVCrwlKZg_6Q"

	ctx := context.Background()
	tmpYoutubeService, err := youtube.NewService(ctx, option.WithAPIKey(ytAPIKey))

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)
	}

	mContentArray := []string{"snippet"}

	tmpSearchListRequest := tmpYoutubeService.Search.List(mContentArray)
	tmpSearchListRequest.Order("date")
	tmpSearchListRequest.MaxResults(25)
	tmpSearchListRequest.ChannelId(channelID)

	tmpSearchListResponse, err := tmpSearchListRequest.Do()

	if err != nil {

		WriteLogMessage("Error: "+err.Error(), true)
	}

	fmt.Println(tmpSearchListResponse)
}

func getDurationByIsoString(isoString string) string {

	var tmpReturnString string

	tmpDuration, _ := duration.FromString(isoString)

	if tmpDuration.Hours > 0 {
		tmpReturnString = strconv.Itoa(tmpDuration.Hours) + "h"
	}
	if tmpDuration.Minutes > 0 {
		tmpReturnString = tmpReturnString + strconv.Itoa(tmpDuration.Minutes) + "m"
	}
	if tmpDuration.Seconds > 0 {
		tmpReturnString = tmpReturnString + strconv.Itoa(tmpDuration.Seconds) + "s"
	}

	return tmpReturnString
}
