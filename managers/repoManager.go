package managers

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/staticvoidlabs/ytagentgo/models"
)

var mVideoRepository models.VideoRepository

func InitRepositories() {

	processConfigFile()

	if len(mChannelList) > 0 {

		// Clear repo slice.
		mVideoRepository.Videos = nil

		// Get videos for current channel list.
		for _, element := range mChannelList {

			tmpVideoSlice, err := GetVideoListByChannelID(element)

			if err != nil {

				logRepoSummary(true)

				return
			}

			for _, e := range tmpVideoSlice {

				mVideoRepository.Videos = append(mVideoRepository.Videos, e)
				mVideoRepository.RefreshedAt = time.Now()
			}

		}

	} else {

		WriteLogMessage("Added 0 videos to repository.", false)

	}

	logRepoSummary(false)
}

func logRepoSummary(errorReceived bool) {

	if errorReceived {

		WriteLogMessage("Added 0 videos to repository.", false)

	} else {

		// mVideoRepository.RefreshedAt: 2022-01-19 14:57:22
		tmpStringDate := mVideoRepository.RefreshedAt.String()[0:10]
		tmpStringTime := mVideoRepository.RefreshedAt.String()[11:19]
		tmpStringDate = strings.Replace(tmpStringDate, "-", "/", -1)

		WriteLogMessage("Added "+strconv.Itoa(len(mVideoRepository.Videos))+" videos to repository at "+tmpStringDate+" "+tmpStringTime, false)

		WriteLogMessage("", false)
		WriteLogMessage("Videos in repository", false)
		WriteLogMessage("--------------------------------------------------------", false)

		for i, e := range mVideoRepository.Videos {

			// Prepare title string
			tmpTitle := ""
			if len(e.Title) > 25 {
				tmpTitle = e.Title[0:25]
			}

			if i == len(mVideoRepository.Videos)-1 {

				WriteLogMessage(e.ChannelTitle+": "+tmpTitle+"... ("+e.Duration+" | "+e.PublishedAt+")", true)

			} else {

				WriteLogMessage(e.ChannelTitle+": "+tmpTitle+"... ("+e.Duration+" | "+e.PublishedAt+")", false)

			}

		}

	}

}

func purgeRepository() {

	mVideoRepository.Videos = nil
	mVideoRepository.RefreshedAt = time.Now()

}

func updateVideoStateInRepo(videoID string, state int) {

	for i, tmpVideo := range mVideoRepository.Videos {

		if tmpVideo.VideoID == videoID {
			mVideoRepository.Videos[i].State = state
		}

	}

}
