package managers

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/sessions"
)

var mIsAuthenticated bool
var mTimeStampLastAccess time.Time

func checkAuthState(w http.ResponseWriter, r *http.Request) bool {

	var err error = nil

	var tmpIsAuthenticated bool = false

	// Generate session key.
	// tmp := securecookie.GenerateRandomKey(32)

	var tmpStore = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

	tmpSession, _ := tmpStore.Get(r, "session_yda")

	tmpMagicKey := tmpSession.Values["magickey"].(string)
	tmpTimeStampString := tmpSession.Values["timestamp"].(string)

	layout := "2006-01-02T15:04:05.000Z"
	tmpTimeStamp, err := time.Parse(layout, tmpTimeStampString)

	if err != nil {
		WriteLogMessage("Error: "+err.Error(), true)
		fmt.Println(err)
	}

	tmpAgeInMinutes := int(time.Since(tmpTimeStamp.Add(time.Hour*-1 + time.Minute*0 + time.Second*0)).Minutes())

	if tmpMagicKey != "genuine" || tmpAgeInMinutes > 100 {
		fmt.Println("Session timed out!")
	} else {
		fmt.Println("Session ok!")
		tmpIsAuthenticated = true
	}

	/*
		// Write session date for debugging.
		tmpSession.Values["magickey"] = "genuine"
		layout := "2006-01-02T15:04:05.000Z"
		tmpSession.Values["timestamp"] = time.Now().Format(layout)
	*/

	err = tmpSession.Save(r, w)

	if err != nil {

		http.Error(w, err.Error(), http.StatusInternalServerError)

		WriteLogMessage("Error: "+err.Error(), true)

		return false
	}

	return tmpIsAuthenticated
}

func authenticateUser(w http.ResponseWriter, r *http.Request, magicKey string) {

	var err error = nil

	var tmpStore = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

	tmpSession, _ := tmpStore.Get(r, "session_yda")

	tmpSession.Values["magickey"] = magicKey
	layout := "2006-01-02T15:04:05.000Z"
	tmpSession.Values["timestamp"] = time.Now().Format(layout)

	err = tmpSession.Save(r, w)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		WriteLogMessage("Error: "+err.Error(), true)
	}

}

func resetSession(w http.ResponseWriter, r *http.Request) {

	var err error = nil

	var tmpStore = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

	tmpSession, _ := tmpStore.Get(r, "session_yda")

	tmpSession.Values["magickey"] = ""
	tmpSession.Values["timestamp"] = ""

	err = tmpSession.Save(r, w)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		WriteLogMessage("Error: "+err.Error(), true)
	}

}
