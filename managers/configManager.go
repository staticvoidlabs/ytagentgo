package managers

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/staticvoidlabs/ytagentgo/models"
)

var mCurrentConfig models.Config
var mChannelList []string
var SessionType string

// Public functions.
func GetCurrentConfig() models.Config {

	return processConfigFile()
}

// Private functions.
func processConfigFile() models.Config {

	var currentConfig models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)
	currentConfig.YoutubeAPIKey = readYoutubeAPIKeyFromFile("./YoutubeApiKey.txt")
	mCurrentConfig = currentConfig

	processChannelIDs()

	return currentConfig
}

func processChannelIDs() {

	tmpChannelListClean := strings.Split(mCurrentConfig.ChannelIDs, ",")

	mChannelList = tmpChannelListClean
}

func GetChannelList() []string {

	return mChannelList
}

func getYoutubeAPIKey() string {

	tmpAPIKey := "n/a"

	if mCurrentConfig.YoutubeAPIKey != "" {
		tmpAPIKey = mCurrentConfig.YoutubeAPIKey
	}

	return tmpAPIKey
}

func getThumbnailCacheDir() string {

	tmpThumbnailCacheDir := "n/a"

	if mCurrentConfig.ThumbailCacheDir != "" {
		tmpThumbnailCacheDir = mCurrentConfig.ThumbailCacheDir
	}

	return tmpThumbnailCacheDir
}

func GetVersionInfo() string {

	tmpVersionInfo := "n/a"

	if mCurrentConfig.Version != "" {
		tmpVersionInfo = mCurrentConfig.Version
	}

	return tmpVersionInfo
}

func readYoutubeAPIKeyFromFile(keyFile string) string {

	tmpKey := "n/a"

	file, err := os.Open(keyFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {

		tmpLine := scanner.Text()

		if tmpLine != "" {
			tmpKey = tmpLine
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return tmpKey
}
