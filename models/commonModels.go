package models

import "time"

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version              string `json:"version"`
	ApiPort              int    `json:"apiPort"`
	UseSSL               bool   `json:"useSSL"`
	DebugLevel           int    `json:"debugLevel"`
	PathDownloads        string `json:"pathDownloads"`
	YtDlLoggingEnabled   bool   `json:"ytDlLoggingEnabled"`
	FullPathYtDl         string `json:"fullPathYtDl"`
	ArgOutput            string `json:"argOutput"`
	ArgTemplateName      string `json:"argTemplateName"`
	CacheDir             string `json:"cacheDir"`
	CertsDir             string `json:"certsDir"`
	ThumbailCacheDir     string `json:"thumbailCacheDir"`
	YoutubeAPIKey        string `json:"ytApiKey"`
	ChannelIDs           string `json:"channelIDs"`
	ShowVideosOfLastDays int    `json:"showVideosOfLastDays"`
	MaxVideosPerChannel  int    `json:"maxVideosPerChannel"`
}

// ChannelList struct holds channelIDs of the Youtube channels of interest.
type ChannelListRaw struct {
	ChannelID string `json:"channelID"`
}

// YtVideo struct defines the data model to hold a video object.
type YtVideo struct {
	ChannelTitle  string
	Title         string
	VideoID       string
	OriginalTitle string
	PublishedAt   string
	Duration      string
	URL           string
	FileName      string
	ThumbURL      string
	ThumbFilepath string
	State         int
}

// YtVideo states: 0=not started | 1=started | 2= finished loading | 3= paused | 4=failed

// VideoRepository struct defines the data model to hold a repository object.
type ViewModelMainSite struct {
	Videos       []YtVideo
	RefreshedAt  time.Time
	Jobs         [10]DownloadJob
	IsJobRunning bool
}

// VideoRepository struct defines the data model to hold a repository object.
type VideoRepository struct {
	Videos      []YtVideo
	RefreshedAt time.Time
}

// Queue struct defines the data model to hold a download job object.
type Queue struct {
	Jobs []DownloadJob
}

// DownloadJob struct defines the data model to hold a download job object.
type DownloadJob struct {
	VideoID         string
	VideoTitle      string
	URL             string
	FilePath        string
	FileExtSet      bool
	FileSize        string
	Priority        int
	State           string
	StateInfo       string
	StateHasChanged bool
}

// JobResult struct defines the data model to hold the download job channel object.
type JobResult struct {
	VideoID  string
	JobState string
}
